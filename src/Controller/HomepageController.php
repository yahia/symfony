<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController {
    /*
     * @Route : permet de créer une route
     * utilistion uniquement de doubles guillements
     * 2 paramètre :
     * URL de la route : URL  saisie dans le navigateur
     * identifiant unique de la route : permet de créer des liens entre route
     * nomenclature de nommage : <nom du contrôller>.<nom de la méthode>
     */

    /**
     * @Route("/", name="homepage.index")
     */
    public function index(Request $request): Response {
        
        /*
         * appel d'une vue twig
         * les vues sont à créer dans le dossier templates
         * les vues sont à stocker dans les dossiers spécifiques
         * chaque vue va hériter  d'une page parente; située à la racine du dossier templates (base.html.twig)
         * 
         * nomenclature de création
         * dans le dossier templates, il est recommandé 
         * de créer un dossier du mêm nom que le contrôleur
         * de créer une vue du même nom que la méthode
         * 
         * méthode render permet d'afficher une vue   
         */

        /*
         * Request : permet de récupérer des informations HTTP
         */
        
        return $this->render('homepage/index.html.twig');


        // return new Response('<em>bonjour</em>');
    }

}
